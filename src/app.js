import React from 'react'
import Grid from 'react-bootstrap/lib/Grid'
import Col from 'react-bootstrap/lib/Col'
import Row from 'react-bootstrap/lib/Row'

import WeatherDisplayContainer from './weatherDisplayContainer'
import CityInput from './cityInput'

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentCity: 'London'
        }

        this.handleCityChanged = this.handleCityChanged.bind(this);
    }

    handleCityChanged(city){
        console.log('Handling change for', city)
        this.setState({currentCity: city})
    }

    render() {
        return (
            <Grid>
                <Row>
                    <Col xs={6} md={4}>
                        <WeatherDisplayContainer city={this.state.currentCity}></WeatherDisplayContainer>
                        <CityInput cityValue={this.state.currentCity} handleSubmit={this.handleCityChanged}></CityInput>
                    </Col>
                </Row>
            </Grid>
        );
    }
}

export default App