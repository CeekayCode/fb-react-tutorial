import React from 'react'
import Panel from 'react-bootstrap/lib/Panel'

import WeatherDisplay from './weatherDisplay'

class WeatherDisplayContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentCity: this.props.city,
            isLoading: false,
            weatherData: {
                temp: '0°C',
                date: getCurrentDate(),
            }
        }
    }
    componentWillReceiveProps(nextProps){


        if(nextProps.city !== this.state.currentCity){
            this.setState({
                ...this.state,
                isLoading: true,
                currentCity: nextProps.city
            })
            setTimeout(() => {
                this.update();
            }, 1000)
           
        }
    }

    update(){
        //const url = `http://api.openweathermap.org/data/2.5/weather?q=${this.state.currentCity}&units=metric&APPID=c89858407e7b2cadc87d30cedf250c4e`;
        const url = 'Foo'
        return this.setState({
                isLoading: false,
                currentCity: this.props.city,
                weatherData: {
                    temp: '0°C',
                    date: getCurrentDate(),
                }
            });
        fetch(url)
            .then((resp) => resp.json())
            .then((data) => {
            
                this.setState({
                    currentCity: data.name,
                    weatherData: {
                        temp: `${data.main.temp}°C`,
                        date: getCurrentDate()
                    }
            })
            
        })
    }

    componentDidMount() {
            this.update();
        
    }

    render() {
        const title = `Current Weather for ${this.state.currentCity}`
        return (
            <Panel header={title} bsStyle="primary">
                {!this.state.isLoading &&
                    <WeatherDisplay
                    temp={this.state.weatherData.temp}
                    date={this.state.weatherData.date}
                    city={this.state.currentCity}></WeatherDisplay>
                }
                {this.state.isLoading &&
                    <h3>Loading...</h3>
                }
                
            </Panel>
        );
    }
}

function getCurrentDate() {
    const now = new Date();
    return `${now.getDate()}.${now.getMonth() + 1}.${now.getFullYear()}`;
}

export default WeatherDisplayContainer