import React from 'react'

const WeatherDisplay = (props) => {
    return (
        <div>
            <h2>{props.temp}</h2>
            <h5>{props.date}</h5>
            <h6>{props.city}</h6>
        </div>
    )
}

export default WeatherDisplay