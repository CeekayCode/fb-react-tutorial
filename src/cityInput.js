import React from 'react'
import Form from 'react-bootstrap/lib/Form'
import FormGroup from 'react-bootstrap/lib/FormGroup'
import ControlLabel from 'react-bootstrap/lib/ControlLabel'
import FormControl from 'react-bootstrap/lib/FormControl'
import Button from 'react-bootstrap/lib/Button'

class cityInput extends React.Component {
    constructor(props) {
      super(props);
      this.state = {value: ''};
  
      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({value: event.target.value});
    }

    handleSubmit(event) {
        event.preventDefault();
        return this.props.handleSubmit(this.state.value);
    }

    render(){
        return (
            <Form inline onSubmit={this.handleSubmit}>
                <FormGroup controlId="formInlineEmail">
                    <ControlLabel>City</ControlLabel>
                    {' '}
                    <FormControl value={this.state.value} onChange={this.handleChange} type="city" placeholder="Berlin"/>
                    <Button type="submit">
                    Get the Weather!
                </Button>
                </FormGroup>            
            </Form>

        )
    }
}


export default cityInput